# Revision history for UnicodeChartGenerator

## 0.1.0.0 -- 2019-10-26

* First version. Released on an unsuspecting world.

## 0.2.0.0 -- 2019-10-26

* Cleaned up code. u+ffef character bug not fixed.

## 1.0.0.0 -- 2019-10-26

* Genuinely fixed the UTF bug. Was powershell encoding problem.

## 1.1.0.0 -- 2019-10-27

* Argument parsing is now pretty and elegant. Removed the UTF fix. (Because powershell be damned!)

## 1.2.0.0 -- 2019-10-27

* Now entirely using Text instead of String.